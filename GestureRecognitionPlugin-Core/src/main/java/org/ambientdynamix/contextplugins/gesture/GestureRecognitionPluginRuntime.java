/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.gesture;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.WindowManager;
import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.gesture.Sgesture;
import com.samsung.android.sdk.gesture.SgestureHand;
import org.ambientdynamix.api.contextplugin.ContextListenerInformation;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;

import java.util.ArrayList;
import java.util.UUID;

/**
 * The plug-in allows apps to easily detect the direction of palm movement in front of a Samsung Galaxy Device.
 * <p/>
 * Info :
 * The feature is only supported on Galaxy S5, Note3 and Note4 devices and since December 2014,
 * Samsung provides no further updates for the SDK for new Samsung mobile devices.
 *
 * @author Shivam Verma
 */


public class GestureRecognitionPluginRuntime extends ContextPluginRuntime {

    private final String TAG = this.getClass().getSimpleName();
    private Context context;
    Sgesture sg;
    ArrayList<UUID> gestureListeners;

    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
        this.setPowerScheme(powerScheme);
        this.context = this.getSecuredContext();
        gestureListeners = new ArrayList<UUID>();
        Log.i(TAG, "Starting gesture recognition plugin");
        sg = new Sgesture();
        try {
            sg.initialize(context);
        } catch (SsdkUnsupportedException se) {
            Log.e(TAG, "Gestures not supported");
            return;
        }

        if (!sg.isFeatureEnabled(Sgesture.TYPE_HAND_PRIMITIVE)) {
            Log.e(TAG, "Gestures not enabled");
            return;
        } else {
            Log.i(TAG, "Gestures are enabled");
        }
    }

    SgestureHand sghand;
    SgestureHand.ChangeListener listener;

    @Override
    public void start() {
        if (sghand == null) {
            sghand = new SgestureHand(context.getMainLooper(), sg);
        }
    }

    @Override
    public void stop() {
        try {
            sghand.stop();
            sghand = null;
            listener = null;
        } catch (IllegalArgumentException e) {

        }
        Log.d(TAG, "Stopped!");
    }

    @Override
    public void destroy() {
        this.stop();
        context = null;

        Log.d(TAG, "Destroyed!");
    }


    @Override
    public void handleContextRequest(final UUID requestId, String contextType) {
        if (contextType.equals(GestureDirectionInfo.CONTEXT_TYPE)) {

            if (listener == null) {
                listener = new SgestureHand.ChangeListener() {
                    @Override
                    public void onChanged(SgestureHand.Info info) {
                        int angle = info.getAngle();
                        Direction direction;
                        WindowManager windowService = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                        int rotation = windowService.getDefaultDisplay().getRotation();
                        switch (rotation) {
                            case Surface.ROTATION_0:
                                direction = Direction.computeDirectionFrom(angle);
                                break;
                            case Surface.ROTATION_90:
                                direction = Direction.computeDirectionFrom(angle + 90);
                                break;
                            case Surface.ROTATION_180:
                                direction = Direction.computeDirectionFrom(angle + 180);
                                break;
                            default:
                                direction = Direction.computeDirectionFrom(angle + 270);
                                break;
                        }

                        sendBroadcastContextEvent(new GestureDirectionInfo(direction.toString()));
                    }
                };
                sghand.start(Sgesture.TYPE_HAND_PRIMITIVE, listener);
            }
        }

    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle config) {
        // Warn that we don't handle configured requests
        Log.w(TAG, "handleConfiguredContextRequest called, but we don't support configuration!");
        // Drop the config and default to handleContextRequest
        handleContextRequest(requestId, contextType);
    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {
        // Not supported
    }

    @Override
    public void setPowerScheme(PowerScheme scheme) {
        // Not supported
    }


    @Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo) {
        gestureListeners.add(listenerInfo.getListenerId());
        return true;
    }

}