# Welcome
This README provides an overview of the `org.ambientdynamix.contextplugins.gesture` plug-in for the [Dynamix Framework](http://ambientdynamix.org). This plug-in allows apps to listen for palm movements in front of a Samsung Device. 

Note that this guide assumes an understanding of the [Dynamix Framework documentation](http://ambientdynamix.org/documentation/).

# Plug-in Requirements
* Due to the limitations of the Samsung APIs, this plug-in is only supported on Galaxy S5, Note 3, and Note 4 devices. 
* Dynamix version 2.1 and higher

# Context Support
* `org.ambientdynamix.contextplugins.gesture.subscribe` subscribe to palm movement direction changes.

# Native App Usage
Integrate the Dynamix Framework within your app as [shown here](http://ambientdynamix.org/documentation/integration/). This guide demonstrates how to connect to Dynamix, create context handlers, register for context support, receive events, interact with plug-ins, and use callbacks. To view a simple example of using Dynamix in your native programs, see [this guide](http://ambientdynamix.org/documentation/native-app-quickstart).

## Add context support for the plug-in.
```
#!java
contextHandler.addContextSupport("org.ambientdynamix.contextplugins.gesture", "org.ambientdynamix.contextplugins.gesture.subscribe", callback, listener);
```
## Handle Events
Once context support is successfully added for the `org.ambientdynamix.contextplugins.gesture.subscribe` type, the supplied listener will start receiving events for palm movements. The events are of type `GestureDirectionInfo`, which includes the direction of palm movement in String format: TOP, BOTTOM, LEFT, RIGHT. 





